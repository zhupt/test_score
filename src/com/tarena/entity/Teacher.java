package com.tarena.entity;

import java.util.List;

public class Teacher {
	private int id;
	private String name;
	private Assess assess;
	private int age;
	private String gender;
	private int userqty;
	private double totalScore;
	private double averageScore;
	private List<String> comments;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Assess getAssess() {
		return assess;
	}
	public void setAssess(Assess assess) {
		this.assess = assess;
	}
	public int getUserqty() {
		return userqty;
	}

	public void setUserqty(int userqty) {
		this.userqty = userqty;
	}

	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public double getTotalScore() {
		return totalScore;
	}
	public void setTotalScore(double totalScore) {
		this.totalScore = totalScore;
	}
	public double getAverageScore() {
		return averageScore;
	}
	public void setAverageScore(double averageScore) {
		this.averageScore = averageScore;
	}
	public List<String> getComments() {
		return comments;
	}
	public void setComments(List<String> comments) {
		this.comments = comments;
	}

}
