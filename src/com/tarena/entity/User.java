package com.tarena.entity;

public class User {
	private int id;
	private String name;
	private String userName;
	private String password;
	private String phone;
	private String comment;
	
	public User(){
		
	}

	public User(String name, String userName, String password,
			String phone) {
		super();
		this.name = name;
		this.userName = userName;
		this.password = password;
		this.phone = phone;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
}
