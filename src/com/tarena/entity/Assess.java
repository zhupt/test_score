package com.tarena.entity;

public class Assess {
	private double express;
	private double arranged;
	private double explain;
	private double interactive;
	private double question;
	
	public Assess(){
		
	}
	

	public Assess(double express, double arranged, double explain,
			double interactive, double question) {
		super();
		this.express = express;
		this.arranged = arranged;
		this.explain = explain;
		this.interactive = interactive;
		this.question = question;
	}


	public double getExpress() {
		return express;
	}

	public void setExpress(double express) {
		this.express = express;
	}

	public double getArranged() {
		return arranged;
	}

	public void setArranged(double arranged) {
		this.arranged = arranged;
	}

	public double getExplain() {
		return explain;
	}

	public void setExplain(double explain) {
		this.explain = explain;
	}

	public double getInteractive() {
		return interactive;
	}

	public void setInteractive(double interactive) {
		this.interactive = interactive;
	}

	public double getQuestion() {
		return question;
	}

	public void setQuestion(double question) {
		this.question = question;
	}
	
}
