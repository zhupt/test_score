package com.tarena.dao;

import java.util.List;

import com.tarena.entity.Manager;
import com.tarena.entity.Teacher;
import com.tarena.entity.User;

public interface UserDAO {
	/**
	 * 根据学生用户名查找用户
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public User findUser(User user)throws Exception;
	/**
	 * 注册学生用户
	 * @param user
	 * @throws Exception
	 */
	public void insertUser(User user)throws Exception;
	/**
	 * 查找所有的项目老师信息
	 * @return
	 * @throws Exception
	 */
	public List<Teacher> findAll()throws Exception;
	/**
	 * 根据老师ID查找老师信息
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public Teacher findTeacher(Teacher teacher)throws Exception;
	/**
	 * 根据老师名字查找老师信息
	 * @param teacher
	 * @return
	 * @throws Exception
	 */
	public Teacher findTeacherName(Teacher teacher)throws Exception;
	/**
	 * 评分完成后向数据表中修改评分结果信息
	 * @param teachers
	 * @param teachers2
	 * @throws Exception
	 */
	public void updateTeacher(Teacher teacher)throws Exception;
	/**
	 * 添加新的项目老师
	 * @param teacher
	 * @throws Exception
	 */
	public void insertTeacher(Teacher teacher)throws Exception;
	/**
	 * 删除项目老师信息
	 * @param teacher
	 * @throws Exception
	 */
	public void deleteTeacher(Teacher teacher)throws Exception;
	/**
	 * 根据管理员名字查找管理员信息（管理员登录）
	 * @param manager
	 * @return
	 * @throws Exception
	 */
	public Manager findManager(Manager manager)throws Exception;
	/**
	 * 检查用户是否评价过这个老师
	 * @param uid
	 * @param tid
	 * @return
	 * @throws Exception
	 */
	public boolean isScored(int uid, int tid) throws Exception;
	/**
	 * 给数据库里插入评价过这个老师的信息
	 * @throws Exception
	 */
	public void insertScored(int uid, int tid,String comment)throws Exception;
	/**
	 * 清空学生与老师关系表
	 * @throws Exception
	 */
	public void init()throws Exception;
	/**
	 * 初始化老师信息
	 * @throws Exception
	 */
	public void updateUT()throws Exception;
	/**
	 * 根据老师id查询所有总体评价信息
	 * @param tid
	 * @return
	 * @throws Exception
	 */
	public List<String> findComments(int tid,int page,int size)throws Exception;
	/**
	 * 统计该老师总体评价的总页数
	 * @param size
	 * @return
	 * @throws Exception
	 */
	public int countTotalPage(int tid,int size)throws Exception;
	
}
