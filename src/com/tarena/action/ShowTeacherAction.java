package com.tarena.action;

import com.tarena.dao.UserDAO;
import com.tarena.dao.UserDAOJdbcImpl;
import com.tarena.entity.Teacher;

public class ShowTeacherAction extends BaseAction{
	private int page = 1;
	private int size = 5;
	private int totalPage;
	private Teacher teacher;
	
	public String execute(){
		UserDAO dao = new UserDAOJdbcImpl();
		try {
			teacher = dao.findTeacher(teacher);
			totalPage = dao.countTotalPage(teacher.getId(), size);
			teacher.setComments(dao.findComments(teacher.getId(),page,size));
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
		return "success";
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}
	
}
