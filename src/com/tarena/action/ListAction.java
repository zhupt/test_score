package com.tarena.action;

import com.tarena.dao.UserDAO;
import com.tarena.dao.UserDAOJdbcImpl;
import com.tarena.entity.Teacher;
import com.tarena.entity.User;

public class ListAction extends BaseAction{
	private Teacher teacher;
	private User user;
	
	public String execute(){
		UserDAO dao = new UserDAOJdbcImpl();
		String comment = user.getComment();
		try {
			Teacher t = dao.findTeacherName(teacher);
			user = dao.findUser(user);
			if(dao.isScored(user.getId(), t.getId())){
				return "message";
			}
			dao.insertScored(user.getId(), t.getId(),comment);
			dao.updateTeacher(teacher);
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
		return "success";
	}


	public Teacher getTeacher() {
		return teacher;
	}


	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}


	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
