package com.tarena.action;

import com.tarena.dao.UserDAO;
import com.tarena.dao.UserDAOJdbcImpl;
import com.tarena.entity.User;

public class CheckNameAction extends BaseAction{
	private User user;
	private boolean ok = false;
	public String execute(){
		UserDAO dao = new UserDAOJdbcImpl();
		try {
			user = dao.findUser(user);
			if(user == null){
				ok = true;
			}else{
				ok = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
		return "success";
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public boolean isOk() {
		return ok;
	}
	public void setOk(boolean ok) {
		this.ok = ok;
	}
	
}
