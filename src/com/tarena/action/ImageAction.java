package com.tarena.action;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Map;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import com.tarena.util.ImageUtil;

public class ImageAction extends BaseAction{
	private InputStream imageStream;
	
	public String execute(){
		Map<String,BufferedImage> map = ImageUtil.createImage();
		String key = map.keySet().iterator().next();
		session.put("key", key);
		System.out.println(key);
		BufferedImage image = map.get(key);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(baos);
		try {
			encoder.encode(image);
			imageStream = new ByteArrayInputStream(baos.toByteArray());
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
		return "success";
	}
	
	public InputStream getImageStream() {
		return imageStream;
	}

	public void setImageStream(InputStream imageStream) {
		this.imageStream = imageStream;
	}


}
