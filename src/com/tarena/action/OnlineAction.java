package com.tarena.action;

public class OnlineAction extends BaseAction{
	 private int count;
	 
	 public String execute(){
		 count = (Integer)context.getAttribute("online");
		 return "success";
	 }

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
	 
}
