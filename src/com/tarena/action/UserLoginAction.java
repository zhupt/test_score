package com.tarena.action;

import com.tarena.dao.UserDAO;
import com.tarena.dao.UserDAOJdbcImpl;
import com.tarena.entity.User;

public class UserLoginAction extends BaseAction{
	private String message;
	private User user;
	
	public String execute(){
		String password = user.getPassword();
		UserDAO dao = new UserDAOJdbcImpl();
		try {
			user = dao.findUser(user);
			if(user != null){
				if(user.getPassword().equals(password)){
					session.put("user", "user");
					session.put("st", user);
					return "success";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
		message = "用户名或密码有误!";
		return "login";
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
