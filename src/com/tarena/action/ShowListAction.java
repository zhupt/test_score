package com.tarena.action;

import java.util.List;

import com.tarena.dao.UserDAO;
import com.tarena.dao.UserDAOJdbcImpl;
import com.tarena.entity.Teacher;

public class ShowListAction {
	private List<Teacher> teachers;
	
	public String execute(){
		UserDAO dao = new UserDAOJdbcImpl();
		try {
			teachers = dao.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
		return "success";
	}

	public List<Teacher> getTeachers() {
		return teachers;
	}

	public void setTeachers(List<Teacher> teachers) {
		this.teachers = teachers;
	}
	
}
