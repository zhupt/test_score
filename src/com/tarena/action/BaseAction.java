package com.tarena.action;

import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.util.ServletContextAware;

public class BaseAction implements SessionAware,ServletRequestAware,
        RequestAware,ServletContextAware{
	protected Map<String,Object> session;
	protected ServletRequest servletRequest;
	protected Map<String,Object> request;
	protected ServletContext context;
	public void setSession(Map<String,Object> session) {
		this.session = session;
		
	}

	public void setServletRequest(HttpServletRequest servletRequest) {
		this.servletRequest = servletRequest;
		
	}

	public void setRequest(Map<String, Object> request) {
		this.request = request;
		
	}

	public void setServletContext(ServletContext context) {
		this.context = context;
		
	}
	

}
