package com.tarena.action;

import com.tarena.dao.UserDAO;
import com.tarena.dao.UserDAOJdbcImpl;
import com.tarena.entity.Teacher;

public class AddTeacherAction {
	private Teacher teacher;
	
	public String execute(){
		UserDAO dao = new UserDAOJdbcImpl();
		System.out.println("mingzi:"+ teacher.getName());
		System.out.println("mingzi:"+ teacher.getAge());
		try {
			dao.insertTeacher(teacher);
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
		return "success";
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}
	
}
