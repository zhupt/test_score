package com.tarena.action;

import com.tarena.dao.UserDAO;
import com.tarena.dao.UserDAOJdbcImpl;
import com.tarena.entity.Teacher;

public class CheckTeacherNameAction extends BaseAction{
	private Teacher teacher;
	private boolean ok = false;
	
	public String execute(){
		UserDAO dao = new UserDAOJdbcImpl();
		try {
			teacher = dao.findTeacherName(teacher);
			if(teacher == null){
				ok = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
		return "success";
	}
	public Teacher getTeacher() {
		return teacher;
	}
	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}
	public boolean isOk() {
		return ok;
	}
	public void setOk(boolean ok) {
		this.ok = ok;
	}

}
