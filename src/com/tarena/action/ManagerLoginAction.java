package com.tarena.action;

import com.tarena.dao.UserDAO;
import com.tarena.dao.UserDAOJdbcImpl;
import com.tarena.entity.Manager;

public class ManagerLoginAction extends BaseAction{
	private String message;
	private Manager manager;
	
	public String execute(){
		UserDAO dao = new UserDAOJdbcImpl();
		String password = manager.getPassword();
		try {
			manager = dao.findManager(manager);
			if(manager != null){
				if(password.equals(manager.getPassword())){
					session.put("user", "user");
					return "success";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
		message = "用户名或密码错误!";
		return "login1";
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Manager getManager() {
		return manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}
	
}
