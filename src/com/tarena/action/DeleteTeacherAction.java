package com.tarena.action;

import com.tarena.dao.UserDAO;
import com.tarena.dao.UserDAOJdbcImpl;
import com.tarena.entity.Teacher;

public class DeleteTeacherAction {
	private Teacher teacher;
	
	public String execute(){
		UserDAO dao = new UserDAOJdbcImpl();
		try {
			dao.deleteTeacher(teacher);
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
		return "success";
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}
	
}
