package com.tarena.action;

import com.tarena.action.BaseAction;

public class CheckImageAction extends BaseAction{
	private String number;
	private boolean ok = false;
	
	public String execute(){
		String number2 = (String)session.get("key");
		if(number.toUpperCase().equals(number2)){
			ok = true;
		}else{
			ok = false;
		}
		return "success";
	}
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
	public boolean isOk() {
		return ok;
	}
	public void setOk(boolean ok) {
		this.ok = ok;
	}
	
}
