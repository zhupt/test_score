package com.tarena.action;

import com.tarena.dao.UserDAO;
import com.tarena.dao.UserDAOJdbcImpl;
import com.tarena.entity.User;

public class UserRegisterAction extends BaseAction{
	private User user;
	
	public String execute(){
		UserDAO dao = new UserDAOJdbcImpl();
		try {
			dao.insertUser(user);
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
		return "success";
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
}
