package com.tarena.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class NowDate extends SimpleTagSupport{

	public void doTag() throws JspException, IOException {
		PageContext pc = (PageContext)this.getJspContext();
		JspWriter out = pc.getOut();
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String time = sdf.format(date);
		out.println(time);
	}
	
}
