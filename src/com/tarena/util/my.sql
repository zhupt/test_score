drop table test_user;
create table test_user(
id int primary key auto_increment,
name varchar(20) unique,
password varchar(40),
phone varchar(20)
);

drop table test_teacher;
create table test_teacher(
id int primary key auto_increment,
name varchar(20) unique,
age int,
gender varchar(2),
userqty int,
totalScore double,
averageScore double
);
insert into test_teacher(name,userqty,age,gender,totalScore,averageScore) 
values('徐菲',0,23,'男',0,0);
insert into test_teacher(name,userqty,age,gender,totalScore,averageScore) 
values('徐菲2',0,23,'男',0,0);
insert into test_teacher(name,userqty,age,gender,totalScore,averageScore) 
values('苍井空',0,23,'女',0,0);
insert into test_teacher(name,userqty,age,gender,totalScore,averageScore) 
values('柯南',0,23,'男',0,0);
drop table test_manager;
create table test_manager(
id int primary key auto_increment,
userName varchar(20) unique,
name varchar(20),
password varchar(40)
);
drop table test_user_teacher;
create table test_user_teacher(
id int primary key auto_increment,
user_id int,
teacher_id int,
comment varchar(100)
);