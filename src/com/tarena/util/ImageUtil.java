package com.tarena.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class ImageUtil {
	private static final String[] chars = {"0", "1", "2", "3", "4", "5", "6",
		"7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "东",
		"南", "西", "北", "中", "发", "白" };
	private static final int SIZE = 4;
	private static final int LINES = 5;
	private static final int WIDTH = 70;
	private static final int HEIGHT = 25;
	private static final int FONT_SIZE = 20;
	
	public static Map<String,BufferedImage> createImage(){
		BufferedImage image = new 
		BufferedImage(WIDTH,HEIGHT,BufferedImage.TYPE_3BYTE_BGR);
		Graphics graphics = image.getGraphics();
		graphics.setColor(Color.LIGHT_GRAY);
		graphics.fillRect(0, 0, WIDTH, HEIGHT);
		Random random = new Random();
		StringBuffer sb = new StringBuffer();
		for(int i = 1; i <= SIZE; i++){
			int n = random.nextInt(chars.length);
			graphics.setColor(new Color(random.nextInt(256),
					random.nextInt(256),random.nextInt(256)));
			graphics.setFont(new Font(null,Font.ITALIC,FONT_SIZE));
			graphics.drawString(chars[n], (i - 1)*WIDTH/SIZE, HEIGHT);
			sb.append(chars[n]);
		}
		for(int i = 1; i <= LINES; i++){
			graphics.setColor(new Color(random.nextInt(256),
					random.nextInt(256),random.nextInt(256)));
			graphics.drawLine(random.nextInt(WIDTH), random.nextInt(HEIGHT),
					random.nextInt(WIDTH),  random.nextInt(HEIGHT));
		}
		Map<String,BufferedImage> map = new HashMap<String, BufferedImage>();
		map.put(sb.toString(), image);
		return map;
	}
}
