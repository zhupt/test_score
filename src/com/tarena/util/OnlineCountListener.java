package com.tarena.util;

import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class OnlineCountListener implements HttpSessionListener,
     ServletContextListener,ServletContextAttributeListener{
	private int count;
	
	public OnlineCountListener(){
		count = 0;
	}
	
	public void sessionCreated(HttpSessionEvent se) {
		count++;
		setContext(se);
		
	}

	public void sessionDestroyed(HttpSessionEvent se) {
		count--;
		setContext(se);
		
	}
	
	public void setContext(HttpSessionEvent se){
		se.getSession().getServletContext().setAttribute("online", count);
	}

	public void contextDestroyed(ServletContextEvent arg0) {
		System.out.println("contextDestroyed()");
	}

	public void contextInitialized(ServletContextEvent arg0) {
		System.out.println("contextInitialized()");
	}

	public void attributeAdded(ServletContextAttributeEvent arg0) {
		System.out.println("attributeAdded()");
	}

	public void attributeRemoved(ServletContextAttributeEvent arg0) {
		System.out.println("attributeRemoved()");
	}

	public void attributeReplaced(ServletContextAttributeEvent arg0) {
		System.out.println("attributeReplaced()");
	}

}
