<%@page pageEncoding="UTF-8" contentType="text/html;charset=utf-8" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="d1" uri="http://java.zpt.com/date"%>
<%@page isELIgnored="false" %>
<html>
	<head>
		<title>登录页面</title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<script type="text/javascript" src="js/jquery-1.4.3.js"></script>
		<script type="text/javascript" src="js/user.js"></script>
	</head>

	<body>
		<div id="wrap">
			<div id="top_content">
				<div id="header">
					<div id="rightheader">
						<p>
							<d1:date/>
							<br />
						</p>
					</div>
					<div id="topheader">
						<h1 id="title">
							<a href="#">Tarena</a>
						</h1>
					</div>
					<div id="navigation">
					</div>
				</div>
				<div id="content">
					<p id="whereami">
					</p>
					<h1>
						 学生登录
					</h1>
					<form action="login.action" method="post" id="userLogin">
						<table cellpadding="0" cellspacing="0" border="0"
							class="form_table">
							<tr>
								<td valign="middle" align="right">
									用户名:
								</td>
								<td valign="middle" align="left">
									<input type="text" class="inputgri" name="user.name" id="name"/>
									<span id="message" style="font-size:10;color:red;">${message}</span>
								</td>
							</tr>
							<tr>
								<td valign="middle" align="right">
									密码:
								</td>
								<td valign="middle" align="left">
									<input type="password" class="inputgri" name="user.password" id="password"/>
								</td>
							</tr>
							<tr>
								<td valign="middle" align="right">
									验证码:
									<img id="image" src="image.action" />
									<a href="javascript:;" id="changeImage">换一张</a>
								</td>
								<td valign="middle" align="left">
									<input type="text" class="inputgri" name="number" id="number"/>
									<span style="color:pink;" id="imageMessage"></span>
								</td>
							</tr>
						</table>
						<p>
							<input type="submit" class="button" value="确认 &raquo;" />
							<input type="reset" class="button" value="重置 &raquo;" />
							<input type="button" class="button" value="注册 &raquo;" onclick="location='user_register.jsp'"; />
						</p>
					</form>
				</div>
			</div>
			<div id="footer">
				<div id="footer_bg">
			      xufei@163.com
				</div>
			</div>
		</div>
	</body>
</html>
