<%@page pageEncoding="UTF-8" contentType="text/html;charset=utf-8" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="d1" uri="http://java.zpt.com/date"%>
<%@page isELIgnored="false" %>
<html>
	<head>
		<title>成员列表</title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<script type="text/javascript" src="js/jquery-1.4.3.js"></script>
		<script type="text/javascript" src="js/main.js"></script>
		<script type="text/javascript">
		    $(function(){
		       var timer = window.setInterval(function(){
		            $.post("online.action",
		            function(count){
		               $("#online").html(count + "");
		            }
		            );
		       },30);
		    });
		     $(function(){
		      $("#exit").click(function(){
		          $.post("user_Exit.action",
		          function(data){
		              if(data){
		                 window.close();
		              }
		          });
		      });
		    });
		</script>
	</head>
	<s:if test="user == null">
	   <jsp:forward page="user_login.jsp"/>
	</s:if>
	<body>
		<div id="wrap">
			<div id="top_content">
				<div id="header">
					<div id="rightheader">
						<p>
							<d1:date/>
							<br />
							在线人数:<span id="online"></span>
						</p>
					</div>
					<div id="topheader">
						<h1 id="title">
							<a href="#">Tarena</a>
						</h1>
					</div>
					<div id="navigation">
					</div>
				</div>

				<div id="content">
					<p id="whereami">
					</p>
					<h1>
						欢迎您为项目老师评分!
					</h1>
					<form action="list.action" method="post" id="submit">
					<input type="hidden" name="user.name" value='<s:property value="user.name"/>'/>
					<table class="table">
						<tr class="table_header">
							<td>
								姓&nbsp;&nbsp;名
							</td>
							<td>
								表达能力<br/>(0-20分)
							</td>
							<td>
								知识准备<br/>(0-20分)
							</td>
							<td>
								知识讲解<br/>(0-20分)
							</td>
							<td>
							   课堂互动<br/>(0-20分)
							</td>
							<td>
							   问题解决<br/>(0-20分)
							</td>
						</tr>
							<tr>
								<td>
									<input type="text" size="10" name="teacher.name" id="name"/>
								</td>
								<td>
									<input type="text" size="10" name="teacher.assess.express" id="a1"/>
								</td>
								<td>
									<input type="text" size="10" name="teacher.assess.arranged" id="a2"/>
								</td>
								<td>
									<input type="text" size="10" name="teacher.assess.explain" id="a3"/>
								</td>
								<td>
									<input type="text" size="10" name="teacher.assess.interactive" id="a4"/>
								</td>
								<td>
									<input type="text" size="10" name="teacher.assess.question" id="a5"/>
								</td>
							</tr>			
					</table>
					<h1>整体评价:<span style="color:red; font-size:10pt;">(100字符以内)</span></h1>
					<textarea rows="3" cols="25" name="user.comment" id="a6"></textarea>
					<span style="font-size:10pt; color:red;" id="message"></span>
					<p>
						<input type="submit" class="button" value="提交"/>
						<input type="button" class="button" value="安全退出" id="exit"/>
					</p>
				   </form>
				</div>
			</div>
			<div id="footer">
				<div id="footer_bg">
					xufei@163.com
				</div>
			</div>
		</div>
	</body>
</html>
