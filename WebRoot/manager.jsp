<%@page pageEncoding="UTF-8" contentType="text/html;charset=utf-8" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="d1" uri="http://java.zpt.com/date"%>
<%@page isELIgnored="false" %>
<html>
	<head>
		<title>成员列表</title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<script type="text/javascript" src="js/jquery-1.4.3.js"></script>
		<script type="text/javascript">
		    $(function(){
		       var timer = window.setInterval(function(){
		            $.post("online.action",
		            function(count){
		               $("#online").html(count + "");
		            }
		            );
		       },30);
		    });
		    $(function(){
		      $("#exit").click(function(){
		          $.post("user_Exit.action",
		          function(data){
		              if(data){
		                 window.close();
		              }
		          });
		      });
		    });
		</script>
	</head>
	<body>
		<div id="wrap">
			<div id="top_content">
				<div id="header">
					<div id="rightheader">
						<p>
							<d1:date/>
							<br />
							在线人数:<span id="online"></span>
						</p>
					</div>
					<div id="topheader">
						<h1 id="title">
							<a href="#">Tarena</a>
						</h1>
					</div>
					<div id="navigation">
					</div>
				</div>

				<div id="content">
					<p id="whereami">
					</p>
					<h1>
						项目经理成绩列表!
					</h1>
					<table class="table">
						<tr class="table_header">
							<td>
								姓名
							</td>
							<td>
								总分数
							</td>
							<td>
								平均分数
							</td>
							<td>
								操作
							</td>
						</tr>
						<s:iterator value="teachers" status="st">
							<tr class='row<s:property value="#st.index % 2 + 1" />'>
								<td>
									<s:property value="name" />
								</td>
								<td>
									<s:property value="totalScore" />
								</td>
								<td>
									<s:property value="averageScore" />
								</td>
								<td>
								    <a href="showTeacher.action?teacher.id=<s:property value='id'/>" >详细</a>
									<a href="deleteTeacher.action?teacher.id=<s:property value='id'/>" 
									onclick="return confirm('确定要删除?');">删除</a>
								</td>
							</tr>
						</s:iterator>
						
					</table>
					<input type="button" class="button" value="添加"
							onclick="location='addTeacher.jsp'" />
					<a href="init.action" onclick="return confirm('确定要重置?');">
					<input type="button" class="button" value="重新测评"/>
					</a>
					<input type="button" class="button" value="安全退出" id="exit"/>
				</div>
			</div>
			<div id="footer">
				<div id="footer_bg">
					xufei@163.com
				</div>
			</div>
		</div>
	</body>
</html>
