<%@page pageEncoding="UTF-8" contentType="text/html;charset=utf-8" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="d1" uri="http://java.zpt.com/date"%>
<%@page isELIgnored="false" %>
<html>
	<head>
		<title>老师详细页面</title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<script type="text/javascript" src="js/jquery-1.4.3.js"></script>
	</head>
	<body>
		<div id="wrap">
			<div id="top_content">
				<div id="header">
					<div id="rightheader">
						<p>
							<d1:date/>
							<br />
						</p>
					</div>
					<div id="topheader">
						<h1 id="title">
							<a href="#">Tarena</a>
						</h1>
					</div>
					<div id="navigation">
					</div>
				</div>

				<div id="content">
					<p id="whereami">
					</p>
					<h1>
						项目老师详细:
					</h1>
					<table class="table">
						<tr class="table_header">
							<td>
								姓名
							</td>
							<td>
								性别
							</td>
							<td>
								年龄
							</td>
							<td>
								平均分数
							</td>
							<td>
								评分人数
							</td>
							<td>
							    总分数
							</td>
						</tr>
							<tr class="row1">
								<td>
								<s:property value="teacher.name"/>
								</td>
								<td>
								 <s:property value="teacher.age"/>
								</td>
								<td>
								<s:property value="teacher.gender"/>
								</td>
								<td>
								<s:property value="teacher.averageScore"/>
								</td>
							   <td>
								<s:property value="teacher.userqty"/>
								</td>
								<td>
								<s:property value="teacher.totalScore"/>
								</td>
							</tr>			
					</table>
					<h1>整体评价:</h1>
					<span style="font-size:20pt:">
					<s:if test="page > 1">
					<a href="showTeacher.action?teacher.id=${teacher.id}&page=${page-1}">上一页</a>
					</s:if>
					<s:else>
					<a>上一页</a>
					</s:else>
					第${page}页|共${totalPage}页
					<s:if test="page < totalPage">
					<a href="showTeacher.action?teacher.id=${teacher.id}&page=${page+1}">下一页</a>
					</s:if>
					<s:else>
					<a>下一页</a>
					</s:else>
					</span>
					<table>	
					<s:iterator value="teacher.comments" var="comment" status="st">		
					<tr class='row<s:property value="#st.index % 2 + 1"/>'>
					<td>
					<s:property value="#st.index + size*(page-1) + 1"/>
					</td>
					<td>
					<s:property value="#comment"/>
					</td>
					</tr>	
					</s:iterator>		
					</table>
					<h1></h1>
					<p>
						<input type="button" class="button" value="返回" onclick="location='showList.action'"/>
					</p>
				</div>
			</div>
			<div id="footer">
				<div id="footer_bg">
					xufei@163.com
				</div>
			</div>
		</div>
	</body>
</html>
