<%@page pageEncoding="utf-8" 
contentType="text/html;charset=utf-8"%>
<%@taglib prefix="d1" uri="http://java.zpt.com/date"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<%@page isELIgnored="false"%>
<html>
	<head>
		<title>添加成员</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<script type="text/javascript" src="js/jquery-1.4.3.js"></script>
		<script type="text/javascript" src="js/teacher.js"></script>
	</head>
	<s:if test='#session.user == null'>
	<jsp:forward page="manager_login.jsp"></jsp:forward>
	</s:if>
	<body>
		<div id="wrap">
			<div id="top_content">
					<div id="header">
						<div id="rightheader">
							<p>
						    <d1:date/>
							 <br/>
							</p>
						</div>
						<div id="topheader">
							<h1 id="title">
								<a href="#">Tarena</a>
							</h1>
						</div>
						<div id="navigation">
						</div>
					</div>
				<div id="content">
					<p id="whereami">
					</p>
					<h1>
						添加成员:
					</h1>
					<form action="addTeacher.action" method="post" id="addTeacher">
						<table cellpadding="0" cellspacing="0" border="0"
							class="form_table">
							<tr>
								<td valign="middle" align="right">
									姓名:
								</td>
								<td valign="middle" align="left">
									<input type="text" class="inputgri" name="teacher.name" id="name"/>
									<span style="color:pink;" id="nameMessage"></span>
								</td>
							</tr>
							<tr>
								<td valign="middle" align="right">
									年龄:
								</td>
								<td valign="middle" align="left">
									<input type="text" class="inputgri" name="teacher.age" id="age"/>
									<span style="color:pink;" id="ageMessage"></span>
								</td>
							</tr>
							<tr>
								<td valign="middle" align="right">
									性别:
								</td>
								<td valign="middle" align="left">
									男<input type="radio" class="inputgri" name="teacher.gender" value="男" checked/>
									女<input type="radio" class="inputgri" name="teacher.gender" value="女"/>
								</td>
							</tr>
						</table>
						<p>
							<input type="submit" class="button" value="确认" />
							<input type="button" class="button" value="返回" onclick="location='showList.action'";/>
						</p>
					</form>
				</div>
			</div>
			<div id="footer">
				<div id="footer_bg">
					xufei@126.com
				</div>
			</div>
		</div>
	</body>
</html>