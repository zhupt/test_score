$(function(window,undefined){
	var formFlag = {"name":false,"age":false};//表单项是否通过检验
	var ok = "<img src='img/checked.gif'/>";
	var error = "<img src='img/unchecked.gif'/>";
	//注册模块
    $(function(){
    	//用户名检验
    	$("#name").blur(function(){
    		formFlag.name = false;
    		if($("#name").val() == ""){
    			$("#nameMessage").html(error + "请输入用户名!");
    			return;
    		}
    		$("#nameMessage").html("正在检查...");
    		var e = $("#name").val();
    		var reg = /(^.{2,6}$)/;
    		if(!reg.test(e)){
    		  $("#nameMessage").html(error + "用户名格式有误!");
    		  return;
    		}
    		$.post("user_CheckTeacherName.action",
    				{"teacher.name":$("#name").val()},
    				function(data,status){
    						if(data){
    						$("#nameMessage").html(ok + "用户名可用!");
    						formFlag.name = true;
    						}else{
    						$("#nameMessage").html(error + "用户名已被使用!");
    						}
    				});
    	});
    	//年龄验证
    	$("#age").blur(function(){
    		formFlag.age = false;
    		if($("#age").val() == ""){
    			$("#ageMessage").html(error + "请输入年龄!");
    			return;
    		}
    		$("#ageMessage").html("正在检查...");
    		var pwd = $("#age").val();
    		var reg = /(^\d{1,3}$)/;
    		if(!reg.test(pwd)){
    			$("#ageMessage").html(error + "年龄格式有误!");
    		}else{
    			$("#ageMessage").html(ok + "年龄格式正确!");
    			formFlag.age = true;
    		}
    	});
        //表单验证通过才允许提交
        $(function(){
        	$("#addTeacher").submit(function(){
        		if(formFlag.name&&formFlag.age){
        			return true;
        		}else{
        			$("#ageMessage").html("");
        			$("#nameMessage").html("信息填写有误!");
        			return false;
        		}
        	});
        });
    });
});