$(function(window,undefined){
	var formFlag = {"name":false,"password":false,"password1":false,"phone":false,"image":false};//表单项是否通过检验
	var ok = "<img src='img/checked.gif'/>";
	var error = "<img src='img/unchecked.gif'/>";
	//注册模块
    $(function(){
    	//用户名检验
    	$("#name").blur(function(){
    		formFlag.name = false;
    		if($("#name").val() == ""){
    			$("#nameMessage").html(error + "请输入用户名!");
    			return;
    		}
    		$("#nameMessage").html("正在检查...");
    		var e = $("#name").val();
    		var reg = /(^.{2,6}$)/;
    		if(!reg.test(e)){
    		  $("#nameMessage").html(error + "用户名格式有误!");
    		  return;
    		}
    		$.post("user_CheckName.action",
    				{"user.name":$("#name").val()},
    				function(data,status){
    						if(data){
    						$("#nameMessage").html(ok + "用户名可用!");
    						formFlag.name = true;
    						}else{
    						$("#nameMessage").html(error + "用户名已被使用!");
    						}
    				});
    	});
    	//电话号码验证
    	$("#phone").blur(function(){
    		formFlag.phone = false;
    		if($("#phone").val() == ""){
    			$("#phoneMessage").html(error + "请输入联系方式!");
    			return;
    		}
    		$("#phoneMessage").html("正在检查...");
    		var pwd = $("#phone").val();
    		var reg = /(^\d{11}$)/;
    		if(!reg.test(pwd)){
    			$("#phoneMessage").html(error + "电话号码格式有误!");
    		}else{
    			$("#phoneMessage").html(ok + "电话号码格式正确!");
    			formFlag.phone = true;
    		}
    	});
    	//密码验证
    	$("#password").blur(function(){
    		formFlag.password = false;
    		if($("#password").val() == ""){
    			$("#passwordMessage").html(error + "请输入密码!");
    			return;
    		}
    		$("#passwordMessage").html("正在检查...");
    		var pwd = $("#password").val();
    		var reg = /(^\w{6,20}$)/;
    		if(!reg.test(pwd)){
    			$("#passwordMessage").html(error + "密码格式有误!");
    		}else{
    			$("#passwordMessage").html(ok + "密码格式正确!");
    			formFlag.password = true;
    		}
    	});
    	$("#password1").blur(function(){
    		formFlag.password1 = false;
    		if($("#password1").val() == ""){
    			$("#password1Message").html(error + "请再次输入密码!");
    		}else if($("#password1").val() == $("#password").val()){
    			$("#password1Message").html(ok + "密码输入正确!");
    			formFlag.password1 = true;
    		}else{
    			$("#password1Message").html(error + "密码输入有误!");
    		}
    	});
    	//验证码检验
    	$("#changeImage").click(function(){
    		$("#image").attr("src","image.action?id=" + new Date().getTime());
    	});
	    $("#number").blur( function() {
	    	formFlag.image = false;
	    	$("#message").html("");
		   if ($("#number").val() == "") {
			  $("#imageMessage").html(error + "请输入验证码!");
			  return;
		   }
		 $.post("user_CheckImage.action",
	       {"number":$("#number").val()},
	          function(data,status){
	              if(data){
	                $("#imageMessage").html(ok + "验证码正确!");
	                formFlag.image = true;
	              }else{
	                $("#imageMessage").html(error + "验证码输入错误!");
	                  }
	           });
	    });
    });
    //表单验证通过才允许提交
    $(function(){
    	$("#userRegister").submit(function(){
    		if(formFlag.name&&formFlag.password&&formFlag.password1&&formFlag.phone&&formFlag.image){
    			return true;
    		}else{
    			return false;
    		}
    	});
    });
    //登录模块
    var formFlag2 = {"name":false,"password":false};
    //用户名格式验证
    $("#name").blur(function(){
    	$("#message").html("");
		formFlag2.name = false;
		if($("#name").val() == ""){
			formFlag2.name = false;
			return;
		}
		var e = $("#name").val();
		var reg = /(^.{2,6}$)/;
		if(!reg.test(e)){
			formFlag2.name = false;
		}else{
			formFlag2.name = true;
		}
       });
		//密码验证
    	$("#password").blur(function(){
    		$("#message").html("");
    		formFlag2.password = false;
    		if($("#password").val() == ""){
    			formFlag2.password = false;
    			return;
    		}
    		var pwd = $("#password").val();
    		var reg = /(^\w{6,20}$)/;
    		if(!reg.test(pwd)){
    			formFlag2.password = false;
    		}else{
    			formFlag2.password = true;
    		}
    	});
        //表单验证通过才允许提交
        $(function(){
        	$("#userLogin").submit(function(){
        		if(formFlag2.name&&formFlag2.password&&formFlag.image){
        			return true;
        		}else{
        			$("#message").html("信息填写有误!");
        			return false;
        		}
        	});
        });
});