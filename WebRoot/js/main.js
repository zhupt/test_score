$(function(window,undefined){
	var formFlag = {"name":false,"a1":false,"a2":false,"a3":false,"a4":false,"a5":false,"a6":true};//表单项是否通过检验
	var ok = "<img src='img/checked.gif'/>";
	var error = "<img src='img/unchecked.gif'/>";
	var flag = false;
	//评分模块
    $(function(){
    	//用户名检验
    	$("#name").blur(function(){
    		formFlag.name = false;
    		if($("#name").val() == ""){
    			$("#message").html(error + "请输入评分老师名!");
    			return;
    		}
    		$("#message").html("正在检查...");
    		var e = $("#name").val();
    		var reg = /(^.{2,5}$)/;
    		if(!reg.test(e)){
    		  $("#message").html(error + "姓名格式有误!");
    		  return;
    		}
    		$.post("user_CheckTeacherName.action",
    				{"teacher.name":$("#name").val()},
    				function(data){
    						if(data){
    					   $("#message").html(error + "没有这位老师!");
    						}else{
    							$("#message").html(ok + "可为该老师评分!");
        						formFlag.name = true;
        						flag = true;
    						}
    				});
    	});
    	//密码验证
    	$("#a1").blur(function(){
    		formFlag.a1 = false;
    		var pwd = $("#a1").val();
    		var reg = /(^1\d\.\d$)|(^\d\.\d$)|(^20\.0$)|(^\d$)|(^1\d$)|(^20$)/;
    		if(reg.test(pwd)){
    			formFlag.a1 = true;
    		}
    	});
    	$("#a2").blur(function(){
    		formFlag.a2 = false;
    		var pwd = $("#a2").val();
    		var reg = /(^1\d\.\d$)|(^\d\.\d$)|(^20\.0$)|(^\d$)|(^1\d$)|(^20$)/;
    		if(reg.test(pwd)){
    			formFlag.a2 = true;
    		}
    	});
    	$("#a3").blur(function(){
    		formFlag.a3 = false;
    		var pwd = $("#a3").val();
    		var reg = /(^1\d\.\d$)|(^\d\.\d$)|(^20\.0$)|(^\d$)|(^1\d$)|(^20$)/;
    		if(reg.test(pwd)){
    			formFlag.a3 = true;
    		}
    	});
    	$("#a4").blur(function(){
    		formFlag.a4 = false;
    		var pwd = $("#a4").val();
    		var reg = /(^1\d\.\d$)|(^\d\.\d$)|(^20\.0$)|(^\d$)|(^1\d$)|(^20$)/;
    		if(reg.test(pwd)){
    			formFlag.a4 = true;
    		}
    	});
    	$("#a5").blur(function(){
    		formFlag.a5 = false;
    		var pwd = $("#a5").val();
    		var reg = /(^1\d\.\d$)|(^\d\.\d$)|(^20\.0$)|(^\d$)|(^1\d$)|(^20$)/;
    		if(reg.test(pwd)){
    			formFlag.a5 = true;
    		}
    	});
    	$("#a6").blur(function(){
    		formFlag.a6 = false;
    		var pwd = $("#a6").val();
    		var reg = /(^.{0,100}$)/;
    		if(reg.test(pwd)){
    			formFlag.a6 = true;
    		}
    	});
    //表单验证通过才允许提交
    $(function(){
    	$("#submit").submit(function(){
    		if(formFlag.name&&formFlag.a1&&formFlag.a2&&formFlag.a3&&formFlag.a4&&formFlag.a5&&formFlag.a6){
    			return true;
    		}else{
    			if(flag){
        			$("#message").html(error + "评价信息不合要求!");
    			}else{
    				$("#message").html(error + "没有这位老师!");
    			}
    			return false;
    		}
    	});
      });
   });
});