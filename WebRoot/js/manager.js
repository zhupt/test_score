$(function(window,undefined){
	var formFlag = {"name":false,"password":false,"image":false};//表单项是否通过检验
	var ok = "<img src='img/checked.gif'/>";
	var error = "<img src='img/unchecked.gif'/>";
    $(function(){
        //登录模块
        //用户名格式验证
        $("#name").blur(function(){
        	$("#message").html("");
    		formFlag.name = false;
    		if($("#name").val() == ""){
    			formFlag.name = false;
    			return;
    		}
    		var e = $("#name").val();
    		var reg = /(^.{2,6}$)/;
    		if(!reg.test(e)){
    			formFlag.name = false;
    		}else{
    			formFlag.name = true;
    		}
           });
    		//密码验证
        	$("#password").blur(function(){
        		$("#message").html("");
        		formFlag.password = false;
        		if($("#password").val() == ""){
        			formFlag.password = false;
        			return;
        		}
        		var pwd = $("#password").val();
        		var reg = /(^\w{6,20}$)/;
        		if(!reg.test(pwd)){
        			formFlag.password = false;
        		}else{
        			formFlag.password = true;
        		}
        	});
    	//验证码检验
    	$("#changeImage").click(function(){
    		$("#image").attr("src","image.action?id=" + new Date().getTime());
    	});
	    $("#number").blur( function() {
	    	formFlag.image = false;
	    	$("#message").html("");
		   if ($("#number").val() == "") {
			  $("#imageMessage").html(error + "请输入验证码!");
			  return;
		   }
		 $.post("user_CheckImage.action",
	       {"number":$("#number").val()},
	          function(data,status){
	              if(data){
	                $("#imageMessage").html(ok + "验证码正确!");
	                formFlag.image = true;
	              }else{
	                $("#imageMessage").html(error + "验证码输入错误!");
	                  }
	           });
	    });
    });
    //表单验证通过才允许提交
    $(function(){
    	$("#managerLogin").submit(function(){
    		if(formFlag.name&&formFlag.password&&formFlag.image){
    			return true;
    		}else{
    			$("#message").html("信息填写有误!");
    			return false;
    		}
    	});
    });
});