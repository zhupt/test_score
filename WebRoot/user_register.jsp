<%@page pageEncoding="UTF-8" contentType="text/html;charset=utf-8" %>
<%@taglib prefix="d1" uri="http://java.zpt.com/date"%>
<%@page isELIgnored="false" %>
<html>
	<head>
		<title>注册页面</title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<script type="text/javascript" src="js/jquery-1.4.3.js"></script>
		<script type="text/javascript" src="js/user.js"></script>
	</head>
	<body>
		<div id="wrap">
			<div id="top_content">
				<div id="header">
					<div id="rightheader">
						<p>
							<d1:date/>
							<br />
						</p>
					</div>
					<div id="topheader">
						<h1 id="title">
							<a href="#">Tarena</a>
						</h1>
					</div>
					<div id="navigation">
					</div>
				</div>

				<div id="content">
					<p id="whereami">
					</p>
					<h1>
						注册
					</h1>
					<form action="register.action" method="post" id="userRegister">
						<table cellpadding="0" cellspacing="0" border="0"
							class="form_table">
							<tr>
								<td valign="middle" align="right">
									用户名:
								</td>
								<td valign="middle" align="left">
									<input type="text" class="inputgri" name="user.name" id="name"/>
									<span style="color:pink;" id="nameMessage"></span>
								</td>
							</tr>
							<tr>
								<td valign="middle" align="right">
									密码:
								</td>
							<td valign="middle" align="left">
									<input type="password" class="inputgri" name="user.password" id="password"/>
									<span style="color:pink;" id="passwordMessage"></span>
								</td>
							</tr>
														<tr>
								<td valign="middle" align="right">
									密码:
								</td>
								<td valign="middle" align="left">
									<input type="password" class="inputgri" name="password" id="password1"/>
									<span style="color:pink;" id="password1Message"></span>
								</td>
							</tr>
							<tr>
								<td valign="middle" align="right">
									电话:
								</td>
								<td valign="middle" align="left">
									<input type="text" class="inputgri" name="user.phone" id="phone"/>
									<span style="color:pink;" id="phoneMessage"></span>
								</td>
							</tr>
							<tr>
								<td valign="middle" align="right">
									验证码:
									<img id="image" src="image.action" />
									<a href="javascript:;" id="changeImage">换一张</a>
								</td>
								<td valign="middle" align="left">
									<input type="text" class="inputgri" name="number" id="number"/>
									<span style="color:pink;" id="imageMessage"></span>
								</td>
							</tr>
						</table>
						<p>
							<input type="submit" class="button" value="确定 &raquo;" />
							<input type="reset" class="button" value="重置 &raquo;" />
						</p>
					</form>
				</div>
			</div>
			<div id="footer">
				<div id="footer_bg">
					xufei@163.com
				</div>
			</div>
		</div>
	</body>
</html>
