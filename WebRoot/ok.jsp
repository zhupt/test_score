<%@page pageEncoding="UTF-8" contentType="text/html;charset=utf-8" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="d1" uri="http://java.zpt.com/date"%>
<%@page isELIgnored="false" %>
<html>
	<head>
		<title>完成页面</title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<script type="text/javascript" src="js/jquery-1.4.3.js"></script>
		<script type="text/javascript">
		    $(function(){
		      $("#exit").click(function(){
		          $.post("user_Exit.action",
		          function(data){
		              if(data){
		                 window.close();
		              }
		          });
		      });
		    });
		</script>
	</head>
	<s:if test='#session.user == null'>
	<jsp:forward page="user_login.jsp"></jsp:forward>
	</s:if>
	<body onload="closed();">
		<div id="wrap">
			<div id="top_content">
				<div id="header">
					<div id="rightheader">
						<p>
							<d1:date/>
							<br />
						</p>
					</div>
					<div id="topheader">
						<h1 id="title">
							<a href="#">Tarena</a>
						</h1>
					</div>
					<div id="navigation">
					</div>
				</div>
				<div id="content">
					<p id="whereami">
					</p>
					<h1>
					</h1>
					<form action="login.action" method="post" id="userLogin">
					<input type="hidden" name="user.name" value='<s:property value="user.name"/>'/>
					<input type="hidden" name="user.password" value='<s:property value="user.password"/>'/>
						<table cellpadding="0" cellspacing="0" border="0"
							class="form_table">
							<tr>
								<td valign="middle" align="right">
								</td>
								<td valign="middle" align="left">
								</td>
							</tr>
							<tr>
								<td valign="middle" align="right">
								<div style="">
								   <div border-left="100;" style="font-size:25; color:red;">
								      恭喜您完成评分！
								   </div>
								</div>
								</td>
								<td valign="middle" align="left">
								</td>
							</tr>
							<tr>
								<td valign="middle" align="right">
								</td>
								<td valign="middle" align="left">
								</td>
							</tr>
						</table>
						<p>
						<input type="submit" class="button" value="继续评分"/>
						<input type="button" class="button" value="安全退出" id="exit"/>
					   </p>
					</form>
				</div>
			</div>
			<div id="footer">
				<div id="footer_bg">
				</div>
			</div>
		</div>
	</body>
</html>
